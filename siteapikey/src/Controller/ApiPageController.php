<?php

/* Make names pace of the controller */

namespace Drupal\siteapikey\Controller;

/* Use class */
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult; 
use Drupal\node\Entity\Node;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Create controller.
 */
class ApiPageController extends ControllerBase{

/**
* {@inheritdoc}
*/
public function ApiPage(){
    $site_config = $this->config('system.site');
    $parameters = \Drupal::request()->getpathInfo();
    $arg  = explode('/',$parameters); 
    $node_exist = \Drupal::entityQuery('node')->condition('nid',$arg[3])->execute();
    
    $api_key_check = $arg[2];
    $db_api_key = \Drupal::state()->get('siteapikey');

    if(count($node_exist) > 0 && ($api_key_check === $db_api_key)){

    	$node_load = Node::load($node_exist[1]);
        $node_title = $node_load->get('title')->getValue()[0]['value'];
        $rows = array('node_title'=>$node_title,'site_api_key'=>$db_api_key);
        header("Content-type:application/json"); 
        $res = json_encode($rows);

         return array(
          '#markup' => $res
         );

    }else{
         throw new AccessDeniedHttpException();
    }

	}
}